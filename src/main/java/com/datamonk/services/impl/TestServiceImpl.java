package com.datamonk.services.impl;


import com.datamonk.services.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {
    @Override
    public String test() {
        return "Hello World.";
    }
}
