package com.datamonk.services;


import org.springframework.stereotype.Service;

@Service
public interface TestService {

    public String test();

}
